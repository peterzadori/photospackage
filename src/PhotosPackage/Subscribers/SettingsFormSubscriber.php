<?php

namespace Packages\PhotosPackage\Subscribers;

use Kdyby\Events\Subscriber;
use movi\Application\UI\Form;
use movi\Forms\Controls\UploadControl;
use Packages\AppPackage\Settings\Settings;
use Packages\PhotosPackage\Services\WatermarkUploader;
use Packages\ShopPackage\Services\BrandingImageUploader;
use Packages\ShopPackage\Services\EmailBannerUploader;

class SettingsFormSubscriber implements Subscriber
{

	/**
	 * @var WatermarkUploader
	 */
	private $watermarkUploader;

	/**
	 * @var Settings
	 */
	private $settings;


	public function __construct(WatermarkUploader $watermarkUploader, Settings $settings)
	{
		$this->watermarkUploader = $watermarkUploader;
		$this->settings = $settings;
	}


	public function getSubscribedEvents()
	{
		return [
			'Packages\AppPackage\Modules\BackModule\Forms\SettingsFormFactory::onConfigure'
		];
	}


	public function onConfigure(Form $form)
	{
        $form->addGroup('Vodoznak')->setOption('order', 11);
        $watermark = $form->addContainer('watermark');
        $image = (new UploadControl('Obrázok'))
            ->setUploader($this->watermarkUploader);

        $image->onDelete[] = function() {
            $settings = $this->settings->getSettings();
            $settings->watermark->image = NULL;

            $this->settings->save($settings);
        };

		$watermark['image'] = $image;
	}

} 