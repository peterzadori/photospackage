<?php

namespace Packages\PhotosPackage\Elements\Photos;

use movi\Application\UI\Form;
use Packages\CmsPackage\Content\ElementSettings;

class PhotosSettingsFactory implements ElementSettings
{

    public function configure(Form $form)
    {
        $form->addText('width', 'Šírka');

        $form->addText('height', 'Výška');

        $form->addSelect('mode', 'Zobrazenie', [
            'thumbs' => 'Štvorce',
            'masonry' => 'Masonry'
        ]);
    }

}