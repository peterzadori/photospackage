<?php

namespace Packages\PhotosPackage\Elements\Photos;

use movi\Model\Query;
use Packages\CmsPackage\Content\ElementControl;

final class PhotosElementControl extends ElementControl
{

	public function beforeRender()
	{
		$this->template->photos = $this->element->getPhotos(function(Query $query) {
            $query->orderBy('@id DESC');
        });
	}

} 