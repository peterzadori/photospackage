<?php

namespace Packages\PhotosPackage\Elements\Photos;

use Packages\CmsPackage\Content\CustomizableElement;
use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\PreviewableElement;

class PhotosElement extends ElementType implements CustomizableElement, PreviewableElement
{

	/** @var \Packages\PhotosPackage\Elements\Photos\IPhotosElementSetupControlFactory */
	private $photosBackendControlFactory;

	/** @var \Packages\PhotosPackage\Elements\Photos\IPhotosElementControlFactory */
	private $photosElementControlFactory;


	public function __construct(IPhotosElementControlFactory $photosElementControlFactory, IPhotosBackendControlFactory $photosBackendControlFactory)
	{
		$this->photosElementControlFactory = $photosElementControlFactory;
		$this->photosBackendControlFactory = $photosBackendControlFactory;
	}


	public function getKey()
	{
		return 'photos';
	}


	public function getLabel()
	{
		return 'Fotky';
	}


	public function getEntity()
	{
		return new PhotosElementEntity();
	}


	public function getFrontendControl()
	{
		return $this->photosElementControlFactory->create();
	}


	public function getBackendControl()
	{
		return $this->photosBackendControlFactory->create();
	}


	public function getIcon()
	{
		return 'fa-picture-o';
	}


	public function getElementSettingsFactory()
	{
		return new PhotosSettingsFactory();
	}

} 