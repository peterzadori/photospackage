<?php

namespace Packages\PhotosPackage\Elements\Photos;

interface IPhotosElementControlFactory
{

	/** @return PhotosElementControl */
	public function create();

} 