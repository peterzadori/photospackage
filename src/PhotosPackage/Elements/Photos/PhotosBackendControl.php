<?php

namespace Packages\PhotosPackage\Elements\Photos;

use movi\Application\UI\Form;
use movi\Components\ImageUploader\ImageUploader;
use movi\Files\FilesManager;
use Nette\Image;
use Packages\CmsPackage\Content\ElementControl;
use movi\Components\Uploadify\Uploadify;
use Packages\CmsPackage\Content\ICustomizableElementSetupControl;
use Packages\PhotosPackage\Model\Facades\PhotosFacade;
use Packages\PhotosPackage\Services\PhotosUploader;
use Nette\Http\FileUpload;

class PhotosBackendControl extends ElementControl
{

	/** @var \Packages\PhotosPackage\Model\Facades\PhotosFacade */
	private $photosFacade;

	/** @var \Packages\PhotosPackage\Services\PhotosUploader */
	private $photosUploader;

    /**
     * @var FilesManager
     */
	private $filesManager;


	public function __construct(PhotosFacade $photosFacade, PhotosUploader $photosUploader, FilesManager $filesManager)
	{
		$this->photosFacade = $photosFacade;
		$this->photosUploader = $photosUploader;
		$this->filesManager = $filesManager;
	}


	public function handleRemove($id)
	{
		$photo = $this->photosFacade->findOne($id);

		// Remove photo
		$this->photosFacade->delete($photo);

		// Update photo order
		foreach ($this->element->photos as $row)
		{
			if ($row->order > $photo->order) {
				$row->order--;
				$this->photosFacade->persist($row);
			}
		}

		$this->redrawControl('photos');
	}


    public function handleRotate($id)
    {
        $photo = $this->photosFacade->findOne($id);
        $key = $photo->file;

        $file = $this->filesManager->read($key);
        $file->setKey(NULL);

        $image = Image::fromFile($file->getPath());
        $image->rotate(-90, 0);

        $file->setContent($image->toString($image::JPEG, 100));

        $this->filesManager->write($file);

        $photo->file = $file->getKey();
        $this->photosFacade->persist($photo);

        $this->redrawControl('photos');
    }


	public function handleOrder(array $photos)
	{
		$elements = $this->presenter->getHttpRequest()->getQuery('elements');

		foreach ($elements as $i => $element)
		{
			$i++;

			$photo = $this->photosFacade->findOne($element);

			$photo->order = $i;

			$this->photosFacade->persist($photo);
		}

		$this->presenter->flashMessage('Poradie fotiek bolo uložené', 'success');
		$this->presenter->sendPayload();
	}


	public function beforeRender()
	{
        $this->element->refresh();

		$this->template->photos = $this->element->photos;
	}


	public function renderPreview()
	{
		$template = $this->template;
		$template->setFile(__DIR__ . '/PhotosBackendControlPreview.latte');

		$template->photos = $this->element->photos;

		$template->render();
	}


    protected function createComponentUpload()
    {
        $uploader = new ImageUploader();

        $uploader->onUpload[] = function(FileUpload $fileUpload) {
            $this->photosUploader->upload($fileUpload, $this->element, []);

            $this->presenter->redrawControl(NULL, false);
            $this->redrawControl('photos');
        };

        return $uploader;
    }

} 