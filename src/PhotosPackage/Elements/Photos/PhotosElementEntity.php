<?php

namespace Packages\PhotosPackage\Elements\Photos;

use LeanMapper\Filtering;
use LeanMapper\Fluent;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\PhotosPackage\Model\Entities\Photo;

/**
 * Class PhotosElementEntity
 * @package Packages\PhotosPackage\Elements\Photos
 *
 * @property Photo[] $photos m:belongsToMany(element_id)
 */
class PhotosElementEntity extends Element
{

    public function getDefaultSettings()
    {
        return [
            'width' => 250,
            'height' => 250
        ];
    }


    public function getPhotos()
    {
        return $this->getValueByPropertyWithRelationship('photos', new Filtering(function(Fluent $fluent) {
            $fluent->orderBy('[order] ASC');
        }));
    }

} 