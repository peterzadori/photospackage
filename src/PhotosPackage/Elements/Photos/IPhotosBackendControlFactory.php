<?php

namespace Packages\PhotosPackage\Elements\Photos;

interface IPhotosBackendControlFactory
{

	/** @return PhotosBackendControl */
	public function create();

} 