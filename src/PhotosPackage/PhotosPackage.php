<?php

namespace Packages\PhotosPackage;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IAssetsProvider;
use movi\Packages\Providers\IConfigProvider;

class PhotosPackage extends CompilerExtension implements IPackage, IConfigProvider, IAssetsProvider
{

    public function getConfigFiles()
    {
        return array(
            __DIR__ . '/Resources/config/package.neon'
        );
    }


    public function getAssets()
    {
        return [
            'css' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/photos.css'
                    ]
                ]
            ],
            'js' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/jsrender.min.js'
                    ]
                ]
            ]
        ];
    }

} 