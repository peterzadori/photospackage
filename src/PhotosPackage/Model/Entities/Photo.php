<?php

namespace Packages\PhotosPackage\Model\Entities;

use Packages\CmsPackage\Model\Entities\Element;
use movi\Model\Entities\FileEntity;
use movi\Model\Entities\IdentifiedEntity;

/**
 * Class Photo
 * @package Packages\PhotosPackage\Model\Entities
 *
 * @property string $file m:size(128) m:file
 * @property string|NULL $name
 * @property int $order = 1
 *
 * @property Element $element m:hasOne
 */
final class Photo extends IdentifiedEntity implements FileEntity
{

} 