<?php

namespace Packages\PhotosPackage\Services;

use Nette\Object;
use Nette\Utils\Image;
use Packages\AppPackage\Settings\Settings;
use Packages\CmsPackage\Model\Entities\Element;
use movi\Files\File;
use movi\Files\FilesManager;
use Packages\PhotosPackage\Model\Entities\Photo;
use Packages\PhotosPackage\Model\Facades\PhotosFacade;
use Nette\Http\FileUpload;

class PhotosUploader extends Object
{

	/** @var \movi\Files\FilesManager */
	private $filesManager;

	private $settings;

	/** @var \Packages\PhotosPackage\Model\Facades\PhotosFacade */
	private $photosFacade;


	public function __construct(FilesManager $filesManager, Settings $settings, PhotosFacade $photosFacade)
	{
		$this->filesManager = $filesManager;
		$this->settings = $settings;
		$this->photosFacade = $photosFacade;
	}


	public function upload(FileUpload $fileUpload, Element $element, array $params)
	{
		if (!$fileUpload->isImage()) return false;

		$image = $fileUpload->toImage();
		$image->resize(1920, 1080, $image::FIT | $image::SHRINK_ONLY); // Resize to HD

		// Watermark
		/*
		if ($this->settings->watermark->image) {
			$watermark = $this->filesManager->read($this->settings->watermark->image, true);
			$watermark = Image::fromString($watermark->getContent());

			$image->place($watermark, '20%', '80%', 50);
		}
		*/

		$file = new File($fileUpload->sanitizedName, $image->toString($image::JPEG, 100));
		$file->setNamespace('photos');

		$this->filesManager->write($file);

		$name = (isset($params['name']) && !empty($params['name']) ? $params['name'] : NULL);

		$photo = new Photo();
		$photo->name = $name;
		$photo->file = $file->getKey();
		$photo->element = $element;
		$photo->order = count($element->photos) + 1;

		$this->photosFacade->persist($photo);

		return $file;
	}

} 