<?php

namespace Packages\PhotosPackage\Services;

use movi\Files\File;
use movi\Files\FilesManager;
use movi\Forms\Controls\IUploader;
use Nette\Http\FileUpload;

class WatermarkUploader implements IUploader
{

    /** @var \movi\Files\FilesManager */
    private $filesManager;


    public function __construct(FilesManager $filesManager)
    {
        $this->filesManager = $filesManager;
    }


    public function upload(FileUpload $fileUpload)
    {
        if (!$fileUpload->isImage()) return false;

        $file = new File($fileUpload->sanitizedName, $fileUpload->contents);
        $file->setNamespace('watermark');

        $this->filesManager->write($file);

        return $file->getKey();
    }

}